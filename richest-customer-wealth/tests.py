from main import Solution


class TestClass:
    def test_maximumWealth(self):
        assert Solution().maximumWealth(accounts=[[1, 2, 3], [3, 2, 1]]) == 6
        assert Solution().maximumWealth(accounts=[[1, 5], [7, 3], [3, 5]]) == 10
        assert Solution().maximumWealth(accounts=[[2, 8, 7], [7, 1, 3], [1, 9, 5]]) == 17
