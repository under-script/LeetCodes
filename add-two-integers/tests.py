from main import Solution


class TestClass:
    def test_sum(self):
        assert Solution().sum(num1=12, num2=5) == 17
        assert Solution().sum(num1=-10, num2=4) == -6
