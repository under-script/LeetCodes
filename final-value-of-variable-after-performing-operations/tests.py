from main import Solution


class TestClass:
    def test_finalValueAfterOperations(self):
        assert Solution().finalValueAfterOperations(operations=["--X", "X++", "X++"]) == 1
        assert Solution().finalValueAfterOperations(operations=["++X", "++X", "X++"]) == 3
        assert Solution().finalValueAfterOperations(operations=["X++", "++X", "--X", "X--"]) == 0
