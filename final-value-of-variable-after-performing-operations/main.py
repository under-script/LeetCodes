from typing import List


class Solution:
    def finalValueAfterOperations(self, operations: List[str]) -> int:
        """Simple version"""
        # result: int = 0
        # for operation in operations:
        #     result += 1 if '+' in operation else -1
        # return result
        """List comprehension version"""
        return sum(1 if '+' in operation else -1 for operation in operations)
