from main import Solution


class TestClass:
    def test_fizzBuzz(self):
        assert Solution().fizzBuzz(n=3) == ["1", "2", "Fizz"]
        assert Solution().fizzBuzz(n=5) == ["1", "2", "Fizz", "4", "Buzz"]
        assert Solution().fizzBuzz(n=15) == ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11",
                                             "Fizz", "13", "14", "FizzBuzz"]
