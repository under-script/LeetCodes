from main import Solution


class TestClass:
    def test_differenceOfSums(self):
        assert Solution().differenceOfSums(n=10, m=3) == 19
        assert Solution().differenceOfSums(n=5, m=6) == 15
        assert Solution().differenceOfSums(n=5, m=1) == -15
