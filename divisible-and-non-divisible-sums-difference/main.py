from typing import List


class Solution:
    def differenceOfSums(self, n: int, m: int) -> int:
        return sum(range(1, n + 1)) - sum(filter(lambda x: x % m == 0, list(range(1, n + 1)))) * 2
