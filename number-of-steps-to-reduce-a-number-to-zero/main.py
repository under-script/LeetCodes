from typing import List


class Solution:
    def numberOfSteps(self, num: int) -> int:
        return sum(1 + num % 2 for num in iter(lambda: num // 2 if num > 0 else 0, 0))
