from main import Solution


class TestClass:
    def test_numberOfSteps(self):
        assert Solution().numberOfSteps(num=14) == 6
        assert Solution().numberOfSteps(num=8) == 4
        assert Solution().numberOfSteps(num=123) == 12
