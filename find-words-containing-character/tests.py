from main import Solution


class TestClass:
    def test_findWordsContaining(self):
        assert Solution().findWordsContaining(words=["leet", "code"], x="e") == [0, 1]
        assert Solution().findWordsContaining(words=["abc", "bcd", "aaaa", "cbc"], x="a") == [0, 2]
        assert Solution().findWordsContaining(words=["abc", "bcd", "aaaa", "cbc"], x="z") == []
