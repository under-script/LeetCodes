from main import Solution


class TestClass:
    def test_convertTemperature(self):
        assert Solution().convertTemperature(celsius=36.50) == [309.65000, 97.70000]
        assert Solution().convertTemperature(celsius=122.11) == [395.26000, 251.79800]
