from main import Solution


class TestClass:
    def test_middleNode(self):
        # Assuming Solution().middleNode() returns the node itself
        # You need to convert the nodes to lists for comparison
        assert self.convert_to_list(Solution().middleNode(head=[1, 2, 3, 4, 5])) == [3, 4, 5]
        assert self.convert_to_list(Solution().middleNode(head=[1, 2, 3, 4, 5, 6])) == [4, 5, 6]

    def convert_to_list(self, node):
        result = []
        while node:
            result.append(node.val)
            node = node.next
        return result
