from typing import List, Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        """Simple and low version"""
        # linkedList: ListNode = []
        # length: int = 0
        # while head is not None:
        #     linkedList.append(head)
        #     head = head.next
        #     length += 1
        # return linkedList[length // 2]

        """Better and faster version"""
        middle: ListNode = head
        end: ListNode = head
        while end is not None and end.next is not None:
            middle = middle.next
            end = end.next.next
        return middle
