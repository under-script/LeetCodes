from main import Solution


class TestClass:
    def test_buildArray(self):
        assert Solution().buildArray(nums=[0, 2, 1, 5, 3, 4]) == [0, 1, 2, 4, 5, 3]
        assert Solution().buildArray(nums=[5, 0, 1, 2, 3, 4]) == [4, 5, 0, 1, 2, 3]
