from main import Solution


class TestClass:
    def test_getConcatenation(self):
        assert Solution().getConcatenation(nums=[1, 2, 1]) == [1, 2, 1, 1, 2, 1]
        assert Solution().getConcatenation(nums=[1, 3, 2, 1]) == [1, 3, 2, 1, 1, 3, 2, 1]
