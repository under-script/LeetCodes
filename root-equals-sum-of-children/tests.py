from main import Solution, TreeNode


class TestClass:
    def test_checkTree(self):
        solution = Solution()
        assert solution.checkTree(TreeNode(val=10, left=TreeNode(4), right=TreeNode(6))) == True
        assert solution.checkTree(TreeNode(val=5, left=TreeNode(3), right=TreeNode(1))) == False
