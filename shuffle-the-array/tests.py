from main import Solution


class TestClass:
    def test_shuffle(self):
        assert Solution().shuffle(nums=[2, 5, 1, 3, 4, 7], n=3) == [2, 3, 5, 4, 1, 7]
        assert Solution().shuffle(nums=[1, 2, 3, 4, 4, 3, 2, 1], n=4) == [1, 4, 2, 3, 3, 2, 4, 1]
        assert Solution().shuffle(nums=[1, 1, 2, 2], n=2) == [1, 2, 1, 2]
