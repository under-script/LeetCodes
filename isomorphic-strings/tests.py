from main import Solution


class TestClass:
    def is_true(self, value):
        return value is True

    def is_false(self, value):
        return value is False

    def testIsIsomorphic1(self):
        assert self.is_false(Solution().isIsomorphic("badc", "baba"))

    def testIsIsomorphic2(self):
        assert self.is_true(Solution().isIsomorphic("egg", "add"))

    def testIsIsomorphic3(self):
        assert self.is_false(Solution().isIsomorphic("foo", "bar"))

    def testIsIsomorphic4(self):
        assert self.is_true(Solution().isIsomorphic("paper", "title"))
