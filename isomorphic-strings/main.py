class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        # Create a hashmap to store character mappings
        charMappingMap: dict = {}
        for i in range(len(s)):
            original: str = s[i]
            replacement: str = t[i]

            if not charMappingMap.get(original):
                if not replacement in charMappingMap.values():
                    charMappingMap[original] = replacement
                else:
                    return False
            else:
                mappedCharacter: str = charMappingMap.get(original)
                if mappedCharacter != replacement:
                    return False
        return True
