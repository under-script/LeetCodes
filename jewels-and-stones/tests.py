from main import Solution


class TestClass:
    def test_numJewelsInStones(self):
        assert Solution().numJewelsInStones(jewels="aA", stones="aAAbbbb") == 3
        assert Solution().numJewelsInStones(jewels="z", stones="ZZ") == 0
