from typing import List


class Solution:
    def numJewelsInStones(self, jewels: str, stones: str) -> int:
        """Simple version"""
        # return sum(1 for stone in stones if stone in jewels)
        """Correct version"""
        return sum(stone in jewels for stone in stones)
