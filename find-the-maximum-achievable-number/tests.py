from main import Solution


class TestClass:
    def test_theMaximumAchievableX(self):
        assert Solution().theMaximumAchievableX(num=4, t=1) == 6
        assert Solution().theMaximumAchievableX(num=3, t=2) == 7
