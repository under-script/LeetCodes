from main import Solution


class TestClass:
    def test_runningSum(self):
        assert Solution().runningSum(nums=[1, 2, 3, 4]) == [1, 3, 6, 10]
        assert Solution().runningSum(nums=[1, 1, 1, 1, 1]) == [1, 2, 3, 4, 5]
        assert Solution().runningSum(nums=[3, 1, 2, 10, 1]) == [3, 4, 6, 16, 17]
