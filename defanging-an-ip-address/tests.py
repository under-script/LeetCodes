from main import Solution


class TestClass:
    def test_defangIPaddr(self):
        assert Solution().defangIPaddr(address="1.1.1.1"
                                       ) == "1[.]1[.]1[.]1"

        assert Solution().defangIPaddr(address="255.100.50.0"
                                       ) == "255[.]100[.]50[.]0"
