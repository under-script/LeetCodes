import pytest

from main import Solution


# Parametrized test for happy path scenarios
@pytest.mark.parametrize("test_input, expected_output, test_id", [
    ([1, 2, 3], [1, 2, 3, 1, 2, 3], "happy_path_basic"),
    ([], [], "happy_path_empty"),
    ([-1, -2, -3], [-1, -2, -3, -1, -2, -3], "happy_path_negative"),
    ([1], [1, 1], "happy_path_single_element"),
    ([0] * 100, [0] * 200, "happy_path_large_input"),
])
def test_getConcatenation_happy_path(test_input, expected_output, test_id):
    # Act
    result = Solution().getConcatenation(test_input)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Parametrized test for edge cases
@pytest.mark.parametrize("test_input, expected_output, test_id", [
    ([None], [None, None], "edge_case_none_element"),
    ([1.5, 2.5], [1.5, 2.5, 1.5, 2.5], "edge_case_floats"),
    (["a", "b"], ["a", "b", "a", "b"], "edge_case_strings"),
])
def test_getConcatenation_edge_cases(test_input, expected_output, test_id):
    # Act
    result = Solution().getConcatenation(test_input)

    # Assert
    assert result == expected_output, f"Failed {test_id}"


# Parametrized test for error cases
@pytest.mark.parametrize("test_input, test_id", [
    (None, "error_case_none_input"),
    (123, "error_case_non_list_input"),
    ([1, "a"], "error_case_mixed_types"),
])
def test_getConcatenation_error_cases(test_input, test_id):
    # Act and Assert
    with pytest.raises(TypeError, match=f"Failed {test_id}"):
        Solution().getConcatenation(test_input)
