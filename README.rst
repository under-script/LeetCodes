.. image:: https://github.com/pytest-dev/pytest/raw/main/doc/en/img/pytest_logo_curves.svg
   :target: https://docs.pytest.org/en/stable/
   :height: 100
   :alt: pytest

.. image:: https://github.com/under-script/LeetCodes/blob/master/leetcode.svg
   :target: https://leetcode.com/problemset/?difficulty=EASY&page=1&sorting=W3sic29ydE9yZGVyIjoiREVTQ0VORElORyIsIm9yZGVyQnkiOiJBQ19SQVRFIn1d
   :height: 100
   :alt: leetcode

------


LeetCode Stats
==============

.. raw:: html

    <p align="center"><img src="https://leetcard.jacoblin.cool/under-script?theme=nord&font=Allerta&ext=heatmap"></p>


Run command::

    $ pytest -q tests.py
    ....                                                                     [100%]
    4 passed in 0.04s


Bugs/Requests
-------------

Please use the `GitHub issue tracker <https://github.com/under-script/LeetCodes/issues>`_ to submit bugs or request features.


License
-------

Copyright Yunusov Abdulmajid and others, 2024.

Distributed under the terms of the `MIT`_ license, My solutions are free and open source software.

.. _`MIT`: https://github.com/under-script/LeetCodes/blob/master/LICENSE
