from typing import List


class Solution:
    def numIdenticalPairs(self, nums: List[int]) -> int:
        """Simple version"""
        # count: int = 0
        # for i, num in enumerate(nums):
        # count += nums[i + 1:].count(num)
        # return count
        """List comprehension version"""
        return sum(nums[i + 1:].count(num) for i, num in enumerate(nums))
