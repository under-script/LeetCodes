from main import Solution


class TestClass:
    def test_numIdenticalPairs(self):
        assert Solution().numIdenticalPairs(nums=[1, 2, 3, 1, 1, 3]) == 4
        assert Solution().numIdenticalPairs(nums=[1, 1, 1, 1]) == 6
        assert Solution().numIdenticalPairs(nums=[1, 2, 3]) == 0
